package com.itt.liferay.constants;

/**
 * @author kratiyag.gupta
 */
public class CrudOperationsPortletKeys {

	public static final String CrudOperations = "CrudOperations";

}