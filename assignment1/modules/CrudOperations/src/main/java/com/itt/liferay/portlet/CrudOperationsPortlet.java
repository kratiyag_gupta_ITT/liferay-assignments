package com.itt.liferay.portlet;

import com.itt.employeeService.model.Employee;
import com.itt.employeeService.model.impl.EmployeeImpl;
import com.itt.employeeService.service.EmployeeLocalServiceUtil;
import com.itt.liferay.constants.CrudOperationsPortletKeys;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.ParamUtil;
import java.io.IOException;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import org.osgi.service.component.annotations.Component;

/**
 * @author kratiyag.gupta
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=CrudOperations Portlet",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + CrudOperationsPortletKeys.CrudOperations,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class CrudOperationsPortlet extends MVCPortlet 
{
	@Override
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException 
	{
		super.doView(renderRequest, renderResponse);
	}
	
	public void addEmployee(ActionRequest actionRequest,ActionResponse actionResponse)
	{
		Employee employee=new EmployeeImpl();
		employee.setFirstName(ParamUtil.getString(actionRequest, "firstName"));
		employee.setLastName(ParamUtil.getString(actionRequest, "lastName"));
		employee.setEmail(ParamUtil.getString(actionRequest, "email"));
		employee.setPosition(ParamUtil.getString(actionRequest, "position"));
		EmployeeLocalServiceUtil.addEmployee(employee);
	
	}
	public void deleteEmployee(ActionRequest actionRequest,ActionResponse actionResponse) throws NumberFormatException, PortalException
	{
		EmployeeLocalServiceUtil.deleteEmployee(Integer.parseInt(ParamUtil.getString(actionRequest, "employeeId")));
		System.out.println(EmployeeLocalServiceUtil.getEmployeesCount());	
		actionResponse.setRenderParameter("mvcPath","/deleteEmployee.jsp");
	}
	public void updateEmployee(ActionRequest actionRequest,ActionResponse actionResponse) throws NumberFormatException, PortalException, IOException
	{
		int employeeId=Integer.parseInt(ParamUtil.getString(actionRequest, "empId"));
		Employee employee=new EmployeeImpl();
		employee.setEmployeeId(employeeId);
		employee.setFirstName(ParamUtil.getString(actionRequest, "firstName"));
		employee.setLastName(ParamUtil.getString(actionRequest, "lastName"));
		employee.setEmail(ParamUtil.getString(actionRequest, "email"));
		employee.setPosition(ParamUtil.getString(actionRequest, "position"));
		EmployeeLocalServiceUtil.updateEmployee(employee);
	}
	public void getEmployeeData(ActionRequest actionRequest,ActionResponse actionResponse) throws PortalException
	{
		int id=Integer.parseInt(ParamUtil.getString(actionRequest, "employeeId"));
		Employee employee= EmployeeLocalServiceUtil.getEmployee(id);
		actionRequest.setAttribute("employeeObject", employee);
		actionResponse.setRenderParameter("mvcPath",
				"/updateEmployee.jsp");
	}
}