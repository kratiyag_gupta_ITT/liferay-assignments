<%@page import="com.itt.employeeService.model.Employee"%>
<%@page import="com.itt.employeeService.service.EmployeeLocalServiceUtil"%>
<%@page import="java.util.List" %>
<%@ include file="/init.jsp" %>

<%List<Employee> employee =EmployeeLocalServiceUtil.getEmployees(0, EmployeeLocalServiceUtil.getEmployeesCount());%>

<table border="1" style="padding: 5px;">
<tr style="padding: 5px;">
<td>Employee Id</td>
<td>First Name</td>
<td>Last Name</td>
<td>Email Id</td>
<td>Position</td>
</tr>
<%for(Employee employeeList:employee){ %>
<tr style="padding: 5px;">
<td><%= employeeList.getEmployeeId()%></td>
<td><%= employeeList.getFirstName()%></td>
<td><%= employeeList.getLastName()%></td>
<td><%= employeeList.getEmail()%></td>
<td><%= employeeList.getPosition()%></td>
</tr>
<%} %>
</table>
<portlet:renderURL var="addEmployee">
<portlet:param name="mvcPath" value="/addEmployee.jsp"/>
</portlet:renderURL>
<portlet:renderURL var="deleteEmployee">
<portlet:param name="mvcPath" value="/deleteEmployee.jsp"/>
</portlet:renderURL>
<portlet:renderURL var="updateEmployee">
<portlet:param name="mvcPath" value="/updateEmployee.jsp"/>
</portlet:renderURL>

<a href="<%=addEmployee.toString()%>">Add Employee</a><br/>
<a href="<%=deleteEmployee.toString()%>">Delete Employee</a><br/>
<a href="<%=updateEmployee.toString()%>">Update Employee</a><br/>
