<%@ include file="/init.jsp" %>
<portlet:actionURL name="addEmployee" var="addEmployeeURL" />
<aui:form action="<%=addEmployeeURL.toString()%>" method="post" name="name">
<aui:input name="firstName" type="text"/>
<aui:input name="lastName" type="text"/>
<aui:input name="email" type="text"/>
<aui:input name="position" type="text"/>
<aui:button name="submit" value="Submit" type="submit" />
</aui:form>

<portlet:renderURL var="deleteEmployee">
<portlet:param name="mvcPath" value="/deleteEmployee.jsp"/>
</portlet:renderURL>
<portlet:renderURL var="updateEmployee">
<portlet:param name="mvcPath" value="/updateEmployee.jsp"/>
</portlet:renderURL>
<portlet:renderURL var="getEmployee">
<portlet:param name="mvcPath" value="/employees.jsp"/>
</portlet:renderURL>

<a href="<%=deleteEmployee.toString()%>">Delete Employee</a><br/>
<a href="<%=getEmployee.toString()%>">Get All Employee</a><br/>