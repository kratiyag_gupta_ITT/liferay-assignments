<%@ include file="/init.jsp" %>
<portlet:actionURL name="deleteEmployee" var="deleteEmployeeURL" />
<aui:form action="<%=deleteEmployeeURL.toString()%>" method="post" name="deleteForm">
<aui:input name="employeeId" type="text"/>
<aui:button name="submit" value="Submit" type="submit" />
</aui:form>

<br/><br/>

<portlet:renderURL var="addEmployee">
<portlet:param name="mvcPath" value="/addEmployee.jsp"/>
</portlet:renderURL>
<portlet:renderURL var="updateEmployee">
<portlet:param name="mvcPath" value="/updateEmployee.jsp"/>
</portlet:renderURL>
<portlet:renderURL var="getEmployee">
<portlet:param name="mvcPath" value="/employees.jsp"/>
</portlet:renderURL>
<a href="<%=addEmployee.toString()%>">Add Employee</a><br/>

<a href="<%=getEmployee.toString()%>">Get All Employee</a><br/>