create table employee (
	employeeId INTEGER not null primary key,
	firstName VARCHAR(75) null,
	lastName VARCHAR(75) null,
	email VARCHAR(75) null,
	position VARCHAR(75) null
);